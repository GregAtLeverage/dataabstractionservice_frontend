/**
 * Created by goat on 2015-03-24.
 */

var $ = require('jquery');




function DaoExec(options){

  var exec_callback_handler = false;
  var exec_callback_context = false;

  var $dfdExec = false;


  if(options && options.cb && typeof options.cb === "function" && options.context){
    exec_callback_handler = options.cb;
    exec_callback_context = options.context;
  }



  this.exec = function()
  {
    var args = Array.prototype.slice.call(arguments);

    if($dfdExec === false){
      $dfdExec = $.Deferred();
    } else if($dfdExec.isResolved){
      $dfdExec = undefined;
      $dfdExec = $.Deferred();
    } else {
      console.log("DaoExec.exec() : the current $dfd object has not been resolved...");
      // FAIL THE CURRENT DFD?
    }


    exec_callback_handler.apply(exec_callback_context, [args, $dfdExec]);

    return $dfdExec.promise();
  }


};



module.exports = DaoExec;
