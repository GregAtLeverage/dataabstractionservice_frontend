/**
 * Created by goat on 2015-03-24.
 */


var _ = require('lodash'),
    $ = require('jquery');


var baseUrl = "http://localhost:8080/api/v1/";
//var baseUrl = "https://dev-api.newtopia.com/api/v1/";



/***
 * @function makeAjaxRequest
 * @description
 *   makes an ajax request using ajax request object
 *   and returns a promise (in this case an xhr)
 */
var makeAjaxRequest = function(ajaxReq){

  console.log("###############################");
  console.log("###      makeAjaxRequest    ###");
  console.log("###############################");
  console.log("==> ", ajaxReq);

  return $.ajax(ajaxReq);
};


/***
 * @class AjaxDriver
 * @description
 *   data abstraction service data driver that
 *   reads/writes data using ajax. All methods
 *   return a promise object.
 */
var AjaxDriver = (function(){
  var pblc = {},
      prvt = {};


  /***
   * @method get
   * @param {config}
   * @description
   *   if uuid is provided in config param will generate
   *   a url to retrieve the entity/record associated with
   *   the uuid.
   *   if uuid is not provided in config param will
   *   retrieve a collection, potentially filtered with
   *   other config options provided (from_date, to_date,
   *   max_results, page, etc.)
   *
   */
  pblc.get = function()
  {
    var args = Array.prototype.slice.call(arguments);
    console.log("AjaxDriver.get() ===> ", arguments);

    var ajaxReq = {
      method: "GET",
      url: false
    };

    if(args && args.length){
      var config = args[0];

      var url = baseUrl;
      if(config.typeClass === "record")
        url += "user/me/";
      url += config.typeClass;

      if(config && config.uuid){
        url += "/" + config.uuid;
      } else {
        url += "?_type=" + config.type;
        if(config && config.startdate && config.enddate){
          url += "&from_date=" + config.startdate;
          url += "&to_date=" + config.enddate;
        }
        if(config && config.maxresults){
          url += "&max_results=" + config.maxresults;
        }
        if(config && config.page){
          url += "&page=" + config.page;
        }
      }

      ajaxReq.url = url;
      //console.log("URL: ", url);

      //return makeAjaxRequest(ajaxReq);

    }

    return makeAjaxRequest(ajaxReq);
  };


  /***
   * @method set
   * @param {config}
   * @description
   *   if config.data is an object, set will update (PUT)
   *   the data if a uuid is present. If no uuid is present
   *   set will create (POST) the data.
   *
   *
   */
  pblc.set = function()
  {
    var args = Array.prototype.slice.call(arguments);
    console.log("AjaxDriver.set() ===> ", arguments);

    var ajaxReq = {
      method: "POST",
      url: false,
      dataType: 'json',
      //contentType: "application/json; charset=utf-8",
      xhrFields : {
        withCredentials: true
      },
      data: {
        _type: false,
        _typeClass: false,
        data: false
      }
    };

    if(args && args.length){
      var config = args[0];

      ajaxReq.data._type = config.type;
      ajaxReq.data._typeClass = config.typeClass;

      var url = baseUrl;
      if(config.typeClass == "record")
        url += "user/me/";
      url += config.typeClass;

      if(config && config.data){
        if(_.isArray(config.data)){

        } else if(typeof config.data === "object"){
          // the data to set is an object... ie. a single entity/record
          // if the object has a uuid then we will update it (PUT), otherwise
          // we will create it (POST)

          if(config.data && config.data.uuid){
            ajaxReq.method = "PUT";
          } else {
            ajaxReq.method = "POST";
          }

          //ajaxReq.data.data = JSON.stringify(config.data);
          ajaxReq.data.data = config.data;
          ajaxReq.data = JSON.stringify(ajaxReq.data);
        }
      }

      ajaxReq.url = url;

      //makeAjaxRequest(ajaxReq);
    }

    //console.log("SET URL: ", url);
    //console.log("SET ajaxReq: ", ajaxReq);

    return makeAjaxRequest(ajaxReq);

  };


  pblc.delete = function()
  {
    var args = Array.prototype.slice.call(arguments);
    console.log("AjaxDriver.delete() ===> ", arguments);


  };


  pblc.search = function()
  {
    var args = Array.prototype.slice.call(arguments);
    console.log("AjaxDriver.search() ===> ", arguments);


  };


  return pblc;
})();


module.exports = AjaxDriver;
