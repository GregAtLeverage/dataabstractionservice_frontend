var React = require('react');

//var DataService = require('../stores/service/DataService');
var ExerciseStore = require('../stores/model/ExerciseStore');


var Home = React.createClass({


  on_click_entity: function(evt)
  {
    ExerciseStore.getEntity("entity_1");
  },

  on_click_entities: function(evt)
  {
    ExerciseStore.getEntitiesForMonth();
  },

  on_click_save: function(evt)
  {

    var record = {
      uuid: "ergesrh",
      name: "running",
      description: "a nice morning run",
      measures:[
        {
          key: "time",
          value: 600,
          unit: "seconds",
          unit_short: "s"
        }
      ]
    };

    ExerciseStore.saveRecord(record);
  },

  render: function() {

    return (
      <div className="hero-unit">
        <h1>'Allo, 'Allo!</h1>
        <button onClick={this.on_click_entity}>Test Entity</button>
        <button onClick={this.on_click_entities}>Test Entities</button>
        <button onClick={this.on_click_save}>Save Record</button>
      </div>
    );
  }
});

module.exports = Home;
