/**
 * Created by goat on 2015-03-24.
 */



var Reflux = require('reflux');
var actions = require('../../actions/actions');
var DataService = require('../service/DataService');


var DAO = DataService.createDataAccessObject({
  type: "exercise",
  typeClass: "entity"
});

var recordDao = DataService.createDataAccessObject({
  type: "exercise",
  typeClass: "record"
});



var ExerciseStore = Reflux.createStore({

  listenables: actions,

  init: function() {
    this.message = '';
  },


  getEntity: function(uuid)
  {
    var self = this;

    DAO.get(uuid)
      .exec()
      .done(function(res){
        console.log("ExerciseStore.getEntity() --DONE-- : ", res);
        self.trigger(res);
      })
      .fail(function(err){
        console.log("ExerciseStore.getEntity() --FAIL-- : ", err);
        self.trigger(err);
      });
  },


  getEntitiesForMonth: function()
  {
    var startDate = (new Date(2015,3,1,0,0,0)).toISOString();

    var self = this;

    DAO.get()
       .betweenDates(startDate)
       .maxResults(10)
       .page(1)
       .exec()
      .done(function(res){
        console.log("ExerciseStore.getEntitiesForMonth() --DONE-- : ", res);
        self.trigger(res);
      })
      .fail(function(err){
        console.log("ExerciseStore.getEntitiesForMonth() --FAIL-- : ", err);
        self.trigger(err);
      });


  },


  saveRecord: function(record)
  {
    var self = this;

    recordDao.set(record)
             .exec()
              .done(function(res){
                console.log("ExerciseStore.saveRecord() --DONE-- : ", res);
                self.trigger(res);
              })
              .fail(function(err){
                console.log("ExerciseStore.saveRecord() --FAIL-- : ", err);
                self.trigger(err);
              });
  }




});




module.exports = ExerciseStore;
