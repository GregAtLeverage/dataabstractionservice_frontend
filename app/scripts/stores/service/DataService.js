/**
 * Created by goat on 2015-03-24.
 */

var DataAbstractionService = require('../../data_abstraction_service/DataAbstractionService');
var AjaxDriver = require('../../data_abstraction_service/driver/ajaxDriver');


var DAS = new DataAbstractionService({
  driver: AjaxDriver
});

module.exports = DAS;
