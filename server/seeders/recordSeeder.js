/**
 * Created by goat on 2015-03-24.
 */




var records = {

  "record_1": {
    uuid: "record_1",
    type: "exercise",
    record: {
      name: "walking",
      description: "a brisk walk",
      measures:[
        {
          key: "time",
          value: 1200,
          unit: "seconds",
          unit_short: "s"
        }
      ]
    }
  },

  "record_2": {
    uuid: "record_2",
    type: "exercise",
    record: {
      name: "running",
      description: "a moderate run",
      measures:[
        {
          key: "time",
          value: 600,
          unit: "seconds",
          unit_short: "s"
        }
      ]
    }
  },

  "record_3": {
    uuid: "record_3",
    type: "exercise",
    record: {
      name: "swimming",
      description: "a brisk swim",
      measures:[
        {
          key: "time",
          value: 1800,
          unit: "seconds",
          unit_short: "s"
        }
      ]
    }
  },

  "record_4": {
    uuid: "record_4",
    type: "exercise",
    record: {
      name: "cycling",
      description: "a nice bicycle ride",
      measures:[
        {
          key: "time",
          value: 1800,
          unit: "seconds",
          unit_short: "s"
        }
      ]
    }
  },

  "record_5": {
    uuid: "record_5",
    type: "exercise",
    record: {
      name: "yoga",
      description: "some karmic yoga",
      measures:[
        {
          key: "time",
          value: 2400,
          unit: "seconds",
          unit_short: "s"
        }
      ]
    }
  },

  "record_6": {
    uuid: "record_6",
    type: "food",
    record: {
      name: "grapes",
      description: "green seedless grapes",
      measures:[
        {
          key: "cups",
          value: 1,
          unit: "cups",
          unit_short: "cups"
        },
        {
          key: "calories",
          value: 80,
          unit: "calories",
          unit_short: "kcal"
        }
      ]
    }
  },

  "record_7": {
    uuid: "record_7",
    type: "food",
    record: {
      name: "orange juice",
      description: "fresh squeezed orange juice",
      measures:[
        {
          key: "cups",
          value: 2,
          unit: "cups",
          unit_short: "cups"
        },
        {
          key: "calories",
          value: 240,
          unit: "calories",
          unit_short: "kcal"
        }
      ]
    }
  },

  "record_8": {
    uuid: "record_8",
    type: "food",
    record: {
      name: "almonds",
      description: "organic almonds",
      measures:[
        {
          key: "cups",
          value: 0.25,
          unit: "cups",
          unit_short: "cups"
        },
        {
          key: "calories",
          value: 90,
          unit: "calories",
          unit_short: "kcal"
        }
      ]
    }
  },

  "record_9": {
    uuid: "record_9",
    type: "food",
    record: {
      name: "chicken breast",
      description: "lean chicken breast meat",
      measures:[
        {
          key: "cups",
          value: 1,
          unit: "cups",
          unit_short: "cups"
        },
        {
          key: "calories",
          value: 220,
          unit: "calories",
          unit_short: "kcal"
        }
      ]
    }
  },

  "record_10": {
    uuid: "record_10",
    type: "food",
    record: {
      name: "pasta salad",
      description: "greek style pasta salad",
      measures:[
        {
          key: "cups",
          value: 1,
          unit: "cups",
          unit_short: "cups"
        },
        {
          key: "calories",
          value: 300,
          unit: "calories",
          unit_short: "kcal"
        }
      ]
    }
  }
};












var RecordSeeder = (function(){
  var pblc = {};

  pblc.getRecords = function()
  {
    return records;
  }

  return pblc;
})();




module.exports = RecordSeeder;

