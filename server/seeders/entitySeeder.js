/**
 * Created by goat on 2015-03-24.
 */




var entities = {

  "entity_1": {
    uuid: "entity_1",
    type: "exercise",
    entity: {
      name: "walking",
      description: "a brisk walk",
      measures:[
        {
          key: "time",
          unit: "seconds",
          unit_short: "s"
        }
      ]
    }
  },

  "entity_2": {
    uuid: "entity_2",
    type: "exercise",
    entity: {
      name: "running",
      description: "a moderate run",
      measures:[
        {
          key: "time",
          unit: "seconds",
          unit_short: "s"
        }
      ]
    }
  },

  "entity_3": {
    uuid: "entity_3",
    type: "exercise",
    entity: {
      name: "swimming",
      description: "a brisk swim",
      measures:[
        {
          key: "time",
          unit: "seconds",
          unit_short: "s"
        }
      ]
    }
  },

  "entity_4": {
    uuid: "entity_4",
    type: "exercise",
    entity: {
      name: "cycling",
      description: "a nice bicycle ride",
      measures:[
        {
          key: "time",
          unit: "seconds",
          unit_short: "s"
        }
      ]
    }
  },

  "entity_5": {
    uuid: "entity_5",
    type: "exercise",
    entity: {
      name: "yoga",
      description: "some karmic yoga",
      measures:[
        {
          key: "time",
          unit: "seconds",
          unit_short: "s"
        }
      ]
    }
  },

  "entity_6": {
    uuid: "entity_6",
    type: "food",
    entity: {
      name: "grapes",
      description: "green seedless grapes",
      measures:[
        {
          key: "cups",
          unit: "cups",
          unit_short: "cups"
        },
        {
          key: "calories",
          unit: "calories",
          unit_short: "kcal"
        }
      ]
    }
  },

  "entity_7": {
    uuid: "entity_7",
    type: "food",
    entity: {
      name: "orange juice",
      description: "fresh squeezed orange juice",
      measures:[
        {
          key: "cups",
          unit: "cups",
          unit_short: "cups"
        },
        {
          key: "calories",
          unit: "calories",
          unit_short: "kcal"
        }
      ]
    }
  },

  "entity_8": {
    uuid: "entity_8",
    type: "food",
    entity: {
      name: "almonds",
      description: "organic almonds",
      measures:[
        {
          key: "cups",
          unit: "cups",
          unit_short: "cups"
        },
        {
          key: "calories",
          unit: "calories",
          unit_short: "kcal"
        }
      ]
    }
  },

  "entity_9": {
    uuid: "entity_9",
    type: "food",
    entity: {
      name: "chicken breast",
      description: "lean chicken breast meat",
      measures:[
        {
          key: "cups",
          unit: "cups",
          unit_short: "cups"
        },
        {
          key: "calories",
          unit: "calories",
          unit_short: "kcal"
        }
      ]
    }
  },

  "entity_10": {
    uuid: "entity_10",
    type: "food",
    entity: {
      name: "pasta salad",
      description: "greek style pasta salad",
      measures:[
        {
          key: "cups",
          unit: "cups",
          unit_short: "cups"
        },
        {
          key: "calories",
          unit: "calories",
          unit_short: "kcal"
        }
      ]
    }
  }
};












var EntitySeeder = (function(){
  var pblc = {};

  pblc.getEntities = function()
  {
    return entities;
  }

  return pblc;
})();



module.exports = EntitySeeder;

