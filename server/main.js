/**
 * Created by goat on 2015-03-24.
 */



var express = require('express'),
  app     = express(),

  FS      = require('fs'),
  PATH    = require('path'),
  UTIL    = require('util'),
  HTTP    = require('http'),
  SERVER  = HTTP.createServer(app);

var _ = require('lodash');


var Entity = require('./entity');


var Config = {
  LISTENPORT: 8686
}



var addToHeader = function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
}





app.use(addToHeader);
//app.use(express.static(PATH.join(__dirname, "../public")));
app.use(express.json());
app.use(express.urlencoded());














//
//
//var UserCredentials = {
//  username: "bob",
//  password: "delta"
//}
//
//
//var UserState = {
//  logged_in: false
//}
//
//
//var DashboardItems = {
//  Nutrition:{
//    title: "Log your nutrition",
//    description: "Record what you've eaten today",
//    urls: {
//      action: "app/tracker/nutrition",
//      add:    "app/tracker/nutrition/add"
//    }
//  },
//  Exercise:{
//    title: "Log your exercise",
//    description: "Record the day's exercises",
//    urls: {
//      action: "app/tracker/exercise",
//      add:    "app/tracker/exercise/add"
//    }
//  },
//  WellBeing:{
//    title: "Log your energy level",
//    description: "Record your energy level for the day",
//    urls: {
//      action: "app/tracker/wellbeing",
//      add:    "app/tracker/wellbeing/add"
//    }
//  }
//};
//
//
//
//
//
//
//
//
//
app.get('/', function(req, res){
  res.setHeader('Content-Type', 'text/html');
  res.end( "<h1>Hello World Node!!!</h1>" );
  //test
});





app.get('/entity/:uuid', function(req, res){
  res.setHeader('Content-Type', 'application/json');
  var uuid = req.params.uuid;
  var entity = Entity.getByUuid(uuid);
  if(entity && entity !== null){
    res.end(JSON.stringify(entity));
  } else {
    res.end(JSON.stringify({
      error: true,
      errorMessage: "Entity does not exist"
    }));
  }
});



app.get('/entity', function(req, res){
  res.setHeader('Content-Type', 'application/json');

  var type = req.query._type;

  if(type && type.length){
    var entities = Entity.getByType(type);
    if(entities && _.size(entities)){
      res.end(JSON.stringify(entities));
    }
  }

  res.end(JSON.stringify({
    error: true,
    errorMessage: "No entities for type"
  }));
});






//
//
//
//app.post('/auth/login', function(req, res){
//  res.setHeader('Content-Type', 'text/html');
//
//  var post = req.body;
//
//  if(post.username === UserCredentials.username && post.password === UserCredentials.password){
//    UserState.logged_in = true;
//  } else {
//    UserState.logged_in = false;
//  }
//
//
//  var response_obj = {
//    logged_in: UserState.logged_in
//  }
//
//  if(UserState.logged_in === false){
//    response_obj.message = "Invalid username/password";
//  }
//
//
//  var encoded = JSON.stringify(response_obj);
//
//  res.setHeader('Content-Type', 'application/json');
//
//  res.end(encoded);
//
//});
//
//
//
//
//app.get('/auth/logout', function(req, res){
//
//  UserState.logged_in = false;
//
//  var response_obj = {
//    success: true,
//    logged_in: UserState.logged_in
//  }
//
//  var encoded = JSON.stringify(response_obj);
//
//  res.setHeader('Content-Type', 'application/json');
//
//  res.end(encoded);
//});
//
//
//
//
//
//app.get('/auth/check', function(req, res){
//
//  UserState.logged_in = false;
//
//  var response_obj = {
//    success: true,
//    logged_in: UserState.logged_in
//  }
//
//  var encoded = JSON.stringify(response_obj);
//
//  res.setHeader('Content-Type', 'application/json');
//
//  res.end(encoded);
//});
//
//
//
//
//
//
//app.get('/user/me/dashboard', function(req, res){
//
//  var obj = {
//    data: DashboardItems
//  };
//
//  var encoded = JSON.stringify(obj);
//
//  res.setHeader('Content-Type', 'application/json');
//
//  res.end(encoded);
//});
//






SERVER.listen(Config.LISTENPORT);
console.log("Server listening on port ", Config.LISTENPORT);
