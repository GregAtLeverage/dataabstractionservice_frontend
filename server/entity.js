/**
 * Created by goat on 2015-03-24.
 */

var _ = require('lodash');
var EntitySeeder = require('./seeders/entitySeeder');



var Entity = (function(){
  var pblc = {},
      prvt = {};

  //prvt.entities = {};
  prvt.entities = EntitySeeder.getEntities();

  //var seedEntities = EntitySeeder.getEntities();
  //_.each(seedEntities, function(val, key){
  //  console.log("KEY[", key, "] ==> ", val);
  //});



  pblc.getByType = function(type)
  {
    var entities = {};
    _.each(prvt.entities, function(val, key){
      if(val && val.type && val.type == type){
        entities[key] = val;
      }
    },this);
    return entities;
  }

  pblc.getByUuid = function(uuid)
  {
    var entity = null;
    _.each(prvt.entities, function(val, key){
      if(val && val.uuid && val.uuid == uuid){
        entity = val;
      }
    },this);
    return entity;
  }





  return pblc;
})();




module.exports = Entity;

